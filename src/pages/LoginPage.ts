class LoginPage {

    page: any;
    loginInput: string = 'div.block-view-on [name="email"]';
    passwordInput: string = 'div.block-view-on [name="password"]';
    loginButton: string = 'div.block-view-on button.btn-login-main';
    policyText: string = 'div.block-view-on a[href="http://www.ecwid.com/privacy-policy.html"]'
    errorMsg: string = 'div.bubble-error div.gwt-HTML';

    constructor(page) {
        this.page = page;
    }

    async waitForLoad(){
        await this.page.waitForSelector(this.policyText);
    }

    async inputLogin(text: string){
        await this.page.waitForSelector(this.loginInput);
        await this.page.hover(this.loginInput);
        await this.page.click(this.loginInput);
        await this.page.type(this.loginInput, text);
    }

    async inputPassword(text: string){
        await this.page.waitForSelector(this.passwordInput);
        await this.page.hover(this.passwordInput);
        await this.page.click(this.passwordInput);
        await this.page.type(this.passwordInput, text);
    }

    async tryLogin(user: string, password: string) {
        await this.page.waitFor(250);
        await this.inputLogin(user);
        await this.inputPassword(password);
        await this.page.hover(this.loginButton);
        await this.page.click(this.loginButton);
    }

    async getErrorMsg(){
        await this.page.waitForSelector(this.errorMsg);
        const textContent = await this.page.evaluate(locator => {
            return document.querySelector(locator).textContent;
        }, this.errorMsg);
        return textContent;
    }

}

module.exports = LoginPage;
