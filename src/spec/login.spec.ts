const puppeteer = require('puppeteer');
const itParam = require('mocha-param');
const { expect }  = require('chai');
const config = require('./config.json');
const loginPage = require('./../pages/LoginPage');

let browser;
let page;

describe('Login testing', () => {

  beforeEach(async () => {
    browser = await puppeteer.launch({headless: config.headless, slowMo: 1});
    page = await browser.newPage();
    await page.goto(config.loginUrl, {
      waitLoad: true, 
      waitNetworkIdle: true
    });
  });

  afterEach(async function() {
    if (this.currentTest.state == 'failed') {
      const date = new Date();
      const fileprefix = date.toISOString().replace(/:/g, '_')
      const screenshot_name = './screenshots/' + this.currentTest.title + '_'+  fileprefix + '_screenshot.png';
      await page.screenshot({path: screenshot_name, fullPage: true});
        console.log('Test ' + this.currentTest.title + ' FAIL!\n Tacking screenshot: ' + screenshot_name)
    }
    await browser.close();
  });

  it('1. Успешная авторизация', async () => {
      const LoginPage = new loginPage(page);
      await LoginPage.tryLogin(config.user, config.password);
      await page.waitForNavigation();
      expect(page.url()).to.contain(config.dashboardUrl);
  });

  it('2. Неудачная авторизация. Этот тест проваливается намеренно', async () => {
      const LoginPage = new loginPage(page);
      await LoginPage.tryLogin(config.user, config.password + 'wrong');
      await page.waitForNavigation();
      expect(page.url()).to.contain(config.dashboardUrl);
  });

  const AuthorizeData = [
    {describe: 'Неудачная авторизация с пустым логином', 
    login: '', pass: config.password, 
    expected: 'Пожалуйста, введите правильный email'},
    
    {describe: 'Неудачная авторизация с несуществующим логином', 
    login: config.user + 'wrong', pass: config.password, 
    expected: 'Нет магазинов с таким email. Если вы не помните email вашего аккаунта, напишите нам на login-issues@ecwid.com.'},
    
    {describe: 'Неудачная авторизация с пустым паролем', 
    login: config.user, pass: '', 
    expected: 'Пожалуйста, введите ваш пароль'},
    
    {describe: 'Неудачная авторизация с неверным паролем', 
    login: config.user, pass: config.password + 'wrong', 
    expected: 'Неправильный пароль. Для восстановления пароля воспользуйтесь ссылкой «Забыли пароль?»'},
  ];

  itParam('${value.describe}', AuthorizeData, async function(value){
    const LoginPage = new loginPage(page);
    await LoginPage.tryLogin(value.login, value.pass);
    const errorMsg = await LoginPage.getErrorMsg();
    expect(errorMsg).to.eql(value.expected);
  });
});
